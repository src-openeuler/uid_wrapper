%undefine __cmake_in_source_build

Name:           uid_wrapper
Version:        1.3.0
Release:        3
Summary:        A wrapper for privilege separation
License:        GPL-3.0-or-later
URL:            https://cwrap.org/
Source0:        https://ftp.samba.org/pub/cwrap/%{name}-%{version}.tar.gz

Patch0:         uid_wrapper-fix-cmocka-1.1.6+-support.patch

BuildRequires:  cmake libcmocka-devel >= 1.1.0

%description
Some projects, such as a file server, need privilege separation to be able to switch
to the user who owns the files and do file operations on their behalf. uid_wrapper
convincingly lies to the application, letting it believe it is operating as root and
even switching between UIDs and GIDs as needed. You can start any application making
it believe it is running as root.

If you load the uid_wrapper and enable it with setting UID_WRAPPER=1 all setuid() and
setgid() calls will work, even as a normal user.

It is possible to start your application as fake root with setting UID_WRAPPER_ROOT=1.

Enable debugging of uid_wrapper itself by setting the UID_WRAPPER_DEBUGLEVEL variable.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Conflicts:      uid_wrapper < 1.3.0-3

%description    devel
This package provides the libraries and includes
necessary for developing against the %{name} library.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DUNIT_TESTING=ON
%cmake_build

%install
%cmake_install

%check
%ctest

%files
%license LICENSE
%doc AUTHORS README.md CHANGELOG
%{_libdir}/libuid_wrapper.so.*

%files devel
%{_libdir}/libuid_wrapper.so
%{_libdir}/cmake/uid_wrapper
%{_libdir}/pkgconfig/uid_wrapper.pc

%files help
%{_mandir}/man1/uid_wrapper.1*

%changelog
* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 1.3.0-3
- adopt to new cmake macro
- force out of source build
- split out devel libraries
- drop tarball signature verify so fast building

* Wed Jun 21 2023 yaoxin <yao_xin001@hoperun.com> - 1.3.0-2
- Fix compilation failure due to cmocka update to 1.1.7

* Tue Feb 14 2023 lilong <lilong@kylinos.cn> - 1.3.0-1
- Upgrade to 1.3.0

* Wed Jul 20 2022 zhangfan <zhangfan4@kylinos.cn> - 1.2.9-1
- Update to 1.2.9

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 1.2.4-5
- Completing build dependencies to fix gcc compiler missing error

* Wed Nov 20 2019 lingsheng <lingsheng@huawei.com> - 1.2.4-4
- Package init
